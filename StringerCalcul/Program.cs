﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.XPath;

namespace StringerCalcul
{
    class Program
    {
        static void Main(string[] args)
        {
            var strCalculBody = "(12+45/(45-5)*-(1)/(-2-2-2*3)*(12,5*5))+1 ()";//Console.ReadLine();//
            var messageCheck = checkInputStr(strCalculBody);
            if (!String.IsNullOrWhiteSpace(messageCheck))
                Console.WriteLine(messageCheck);
            else
                Console.WriteLine(Calcul(strCalculBody));

            Console.ReadLine();
        }
        static string Calcul(string expression)
        {
            expression = string.Format("number({0})", expression.Replace("/", " div ").Replace(",", "."));
            try
            {
                return new XPathDocument(new StringReader("<e/>"))
                        .CreateNavigator()
                        .Evaluate(expression).ToString();
            }
            catch
            {
                return "Не Удалось вычислить введенную строку ";
            }
        }
        static string checkInputStr(string expression)
        {
            var message = "";
            //Отслеживать числа с пробелами
            if (new Regex(@"[0-9]+[ ]+[0-9]+").IsMatch(expression))
                message += " Есть числа с пробелами \r\n";

            //Отслеживать не матиматику (символы)
            if (new Regex(@"[^0-9\(\)\+\-\*\/\,\. ]+").IsMatch(expression))
                message += " Есть есть текстовые символы \r\n";

            //Отслеживать деление на 0
            if (new Regex(@"\/[ ]{0,}0").IsMatch(expression))
                message += " Попытка поделить на ноль \r\n";

            //незакрытые скобки
            if (!AllClosing(new Regex(@"[^\(\)]+").Replace(expression, "")))
                 message += " Есть незакрыты скобки  или нарушен порядок\r\n";

            //для сдвоенных операндов отловить нехватку числа
            if (new Regex(@"[\*\/\+\-]{1}[ ]{0,}[\*\/\+\-]{1}").IsMatch(expression)
                ||new Regex(@"^[ ]{0,}[\*\/\+\-]{1}").IsMatch(expression)
                || new Regex(@"\([ ]{0,}[\*\/]{1}").IsMatch(expression)
                || new Regex(@"[\*\/\+\-]{1}[ ]{0,}$").IsMatch(expression)
                || new Regex(@"[\*\/\+\-]{1}[ ]{0,}\)").IsMatch(expression))
                message += " Для сдвоенных оерандов не хватает числа \r\n";
            

            //отслеживать пустые скобки
            if (new Regex(@"\([ ]{0,}\)").IsMatch(expression))
                message += " Есть пустые скобки \r\n";

            return message;
        }
        static bool AllClosing(string strParenthesis)
        {
            var countOpen = 0;
            
            foreach (var itemchar in strParenthesis)
            {
                if (itemchar == '(')
                    countOpen++;
                else
                    countOpen--;

                if (countOpen < 0)
                    return false;

            }
            if (countOpen > 0)
                return false;
            return true;
        }
    }
}

//class Program
//{
//    static void Main(string[] args)
//    {
//        var strCalculBody = "(12+45/(45-5)*-(1)/(-2-2-2*3)*(12,5*5))+1";// Console.ReadLine();

//        var regForFindPiece = new Regex(@"\(([^\(\)]{0,})\)");

//        //Добавить проверку
//        strCalculBody = strCalculBody.Replace(" ", "");
//        while (regForFindPiece.IsMatch(strCalculBody))
//        {
//            strCalculBody = strCalculBody.Replace("--", "").Replace("-+", "-").Replace("+-", "-").Replace("++", "+");
//            Console.WriteLine(strCalculBody);

//            strCalculBody = regForFindPiece.Replace(strCalculBody, (x) => { return SpreadingCalculate(x.Groups[1].Value); });
//        }

//        Console.WriteLine(SpreadingCalculate(strCalculBody));
//        Console.ReadLine();
//    }
//    static string SpreadingCalculate(string longExpreStr)
//    {
//        var regForSpreadingFirstPriority = new Regex(@"[\-\+]{0,1}[0-9\.\,]+[\*\/]{1}[\-\+]{0,1}[0-9\.\,]+");
//        while (regForSpreadingFirstPriority.IsMatch(longExpreStr))
//            longExpreStr = regForSpreadingFirstPriority.Replace(longExpreStr, (x) => { return singleCalculate(x.Value); });

//        var regForSpreadingSecondPriority = new Regex(@"[\-\+]{0,1}[0-9\.\,]+[\+\-]{1}[0-9\.\,]+");
//        while (regForSpreadingSecondPriority.IsMatch(longExpreStr))
//            longExpreStr = regForSpreadingSecondPriority.Replace(longExpreStr, (x) => { return singleCalculate(x.Value); });

//        return singleCalculate(longExpreStr);
//    }
//    static string singleCalculate(string expreStr)
//    {

//        var result = "";
//        var regV2 = new Regex(@"^([\-\+]{0,1})([0-9\.\,]+)$");
//        var regV4 = new Regex(@"^([\-\+]{0,1})([0-9\.\,]+)([\-\+\*\/]{1})([\-\+]{0,1})([0-9\.\,]+)$");

//        if (regV2.IsMatch(expreStr))
//        {
//            var findedPiece = regV2.Match(expreStr);
//            var sign = findedPiece.Groups[1].Value;//sign (+ -)
//            var dig = parseDig(findedPiece.Groups[2].Value);//Dig
//            if (sign == "-")
//                result = (-1 * dig).ToString();
//            else
//                result = (dig).ToString();
//        }
//        else if (regV4.IsMatch(expreStr))
//        {
//            var findedPiece = regV4.Match(expreStr);
//            var firstSign = findedPiece.Groups[1].Value;//(-+)
//            var firstDig = parseDig(findedPiece.Groups[2].Value);//Dig
//            var secondSign = findedPiece.Groups[3].Value;//(-+*/)
//            var subSecondSign = findedPiece.Groups[4].Value;//(-+*/)
//            var secondDig = parseDig(findedPiece.Groups[5].Value);//Dig

//            var aditionApperand = 1;
//            var secondAditionApperand = 1;

//            if (firstSign == "-")
//                aditionApperand = -1;

//            if (subSecondSign == "-")
//                secondAditionApperand = -1;


//            if (secondSign == "-")
//                result = ((aditionApperand * firstDig) - (secondAditionApperand * secondDig)).ToString();
//            else if (secondSign == "+")
//                result = ((aditionApperand * firstDig) + (secondAditionApperand * secondDig)).ToString();
//            else if (secondSign == "*")
//                result = ((aditionApperand * firstDig) * (secondAditionApperand * secondDig)).ToString();
//            else if (secondSign == "/")
//                result = ((aditionApperand * firstDig) / (secondAditionApperand * secondDig)).ToString();
//        }
//        else
//            throw new Exception("Не удалось распознать как высчитать (" + expreStr + ")");
//        Console.WriteLine("SUB calcul " + expreStr + " = " + result);
//        return (result.Contains("-") ? result : "+" + result);
//    }
//    static Decimal parseDig(string dirtDig)
//    {
//        dirtDig = dirtDig.Replace(".", ",");
//        Decimal returnedDig = 0;
//        Decimal.TryParse(dirtDig, out returnedDig);
//        return returnedDig;

//    }
//}