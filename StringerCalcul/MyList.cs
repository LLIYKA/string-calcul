﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringerCalcul
{

    class MyList<T> : IList<T>
    {
        private List<T> StoreDynamicArray = new List<T>();

        public T this[int index]
        {
            get
            {
                return StoreDynamicArray[index];

            }
            set
            {
                StoreDynamicArray[index] = value;
            }
        }
        public int IndexOf(T item)
        {
            return StoreDynamicArray.IndexOf(item);
        }
        public void Insert(int index, T item)
        {
            StoreDynamicArray.Insert(index, item);
        }
        public void RemoveAt(int index)
        {
            StoreDynamicArray.RemoveAt(index);
        }
        public void Add(T item)
        {
            StoreDynamicArray.Add(item);
        }
        public void Clear()
        {
            StoreDynamicArray.Clear();
        }
        public bool Contains(T item)
        {
            return StoreDynamicArray.Contains(item);
        }
        public void CopyTo(T[] item, int index)
        {
            StoreDynamicArray.CopyTo(item, index);
        }
        public bool Remove(T item)
        {
            return StoreDynamicArray.Remove(item);
        }
        public int Count
        {
            get
            {
                return StoreDynamicArray.Count();
            }
        }
        public bool IsReadOnly
        {
            get
            {
                return ((ICollection<T>)StoreDynamicArray).IsReadOnly;
            }
        }
        public IEnumerator<T>  GetEnumerator()
        {
            return StoreDynamicArray.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return StoreDynamicArray.GetEnumerator();
        }
    }
}
